<img alt="GitHub followers" src="https://img.shields.io/github/followers/N1cholasSmith?style=social">     <img alt="GitHub language count" src="https://img.shields.io/github/languages/count/N1cholasSmith/horiseon-search-engine-optimization?style=social">     <img alt="GitHub commit activity" src="https://img.shields.io/github/commit-activity/w/N1cholasSmith/horiseon-search-engine-optimization?style=social">


# Travel-Planner

---
## Description
The Travel Planner is an essential tool for planning a successful holiday, honeymoon or weekend getaway! The Travel Planner utilizes cutting edge features such as weather and travel data. Whilst The Travel Planner was designed with a minimalistic approach to avoid decision paralysis and streamline your holiday experience before you have even left the house.



[travelplanner.com](https://n1cholassmith.github.io/travel-planner/)

---
## Table of Contents
- [Usage] (#usage)
- [Technologies-Used] (#Technologies-Used)
- [Credits/Contributors] (#credits/contributors)
- [How-to-Contribute] (#how-to-contribute)
- [License] (#license)
---
## Usage

The Travel Planner is easily naviagted through the Nav Bar to access all the required information for a holiday

![TravelPlannerHomepage](/assets/images/homepage.png)

![TravelPlannerCheckout](/assets/images/checkout-screen.png)   
---
## Technologies-Used
HTML, CSS And JavaScript were all pivital in the success of this quiz however it heavily relys on the JavaScript Logic for the successful operation.

---
## Credits/Contributors
- Nicholas Smith: LSATA Aircraft Technician & Junior Full Stack Web Development

---
## How to Contribute

Please refer to link for guidelines & Contributor Covenant Code Of Conduct [TravelPlanner](https://www.contributor-covenant.org/)

---
## License
MIT License

Copyright (c) [2021] [TravelPlanner]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

---
